package com.worshop.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worshop.dao.IExamenDAO;
import com.worshop.models.Examen;

import com.worshop.service.IExamenService;
@Service
public class ExamenService implements IExamenService{
	@Autowired
	IExamenDAO service;
	public Examen persist(Examen e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public List<Examen> getAll() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public Examen findById(Integer id) {
		// TODO Auto-generated method stub
		return service.findOne(id);
	}

	@Override
	public Examen merge(Examen e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		service.delete(id);
	}

}
