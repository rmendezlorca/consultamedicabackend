package com.worshop.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worshop.dao.IMedicoDAO;
import com.worshop.models.Medico;
import com.worshop.service.IMedicoService;
@Service
public class MedicoService implements IMedicoService{
	@Autowired
	IMedicoDAO service;
	public Medico persist(Medico e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public List<Medico> getAll() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public Medico findById(Integer id) {
		// TODO Auto-generated method stub
		return service.findOne(id);
	}

	@Override
	public Medico merge(Medico e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		service.delete(id);
}
}