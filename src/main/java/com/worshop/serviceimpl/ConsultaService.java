package com.worshop.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.worshop.dao.IConsultaDAO;
import com.worshop.models.Consulta;
import com.worshop.models.DetalleConsulta;
import com.worshop.service.IConsultaService;
@Service
public class ConsultaService implements IConsultaService{
	@Autowired
	IConsultaDAO service;
	public Consulta persist(Consulta e) {
		// TODO Auto-generated method stub
		e.getDetalleconsulta().forEach(x->x.setConsulta(e));
		Consulta tt= service.save(e);		
		return tt;
		
	}

	@Override
	public List<Consulta> getAll() {
		// TODO Auto-generated method stub
		return service.findAll();
	}

	@Override
	public Consulta findById(Integer id) {
		// TODO Auto-generated method stub
		return service.findOne(id);
	}

	@Override
	public Consulta merge(Consulta e) {
		// TODO Auto-generated method stub
		return service.save(e);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		service.delete(id);
	}

}
