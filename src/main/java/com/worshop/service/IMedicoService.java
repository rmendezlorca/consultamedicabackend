package com.worshop.service;

import java.util.List;

import com.worshop.models.Medico;

public interface IMedicoService {
	Medico persist(Medico e);
	List<Medico> getAll();
	Medico findById(Integer id);
	Medico merge(Medico e);
	void delete(Integer id);

}
