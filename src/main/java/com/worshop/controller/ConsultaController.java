package com.worshop.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.worshop.models.Consulta;
import com.worshop.service.IConsultaService;
@RestController
@RequestMapping("/consultas")
public class ConsultaController {
	@Autowired
	IConsultaService service;
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Consulta>> listar(){
		List<Consulta> especialistas=new ArrayList<>();
		especialistas=service.getAll();		
		return new ResponseEntity<List<Consulta>>(especialistas, HttpStatus.OK);		
		
	}
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public Consulta registrar(@RequestBody Consulta v) {
		return service.persist(v);
	}
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Consulta> listarId(@PathVariable("id") Integer id){		
		Consulta especialista=service.findById(id);		
		return new ResponseEntity<Consulta>(especialista, HttpStatus.OK);		
		
	}
	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object>actualizar(@Valid @RequestBody Consulta especialidad){
		service.merge(especialidad);
		return new ResponseEntity<Object>(HttpStatus.OK);
		
	}
	@DeleteMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id) {
		Consulta espec=service.findById(id);
		/*if(espec==null) {
			throw new ModeloNotFoundException("ID :"+id);
		}
		else*/
		//{
			service.delete(id);
		//}
	}
}
