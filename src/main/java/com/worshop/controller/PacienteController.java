package com.worshop.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.worshop.models.Paciente;
import com.worshop.service.IPacienteService;
@RestController
@RequestMapping("/pacientes")
public class PacienteController {
	@Autowired
	IPacienteService service;
	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Paciente>> listar(){
		List<Paciente> especialistas=new ArrayList<>();
		especialistas=service.getAll();		
		return new ResponseEntity<List<Paciente>>(especialistas, HttpStatus.OK);		
		
	}
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public Paciente registrar(@RequestBody Paciente v) {
		return service.persist(v);
	}
	@GetMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Paciente> listarId(@PathVariable("id") Integer id){		
		Paciente especialista=service.findById(id);		
		return new ResponseEntity<Paciente>(especialista, HttpStatus.OK);		
		
	}
	@PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object>actualizar(@Valid @RequestBody Paciente especialidad){
		service.merge(especialidad);
		return new ResponseEntity<Object>(HttpStatus.OK);
		
	}
	@DeleteMapping(value="/{id}",produces=MediaType.APPLICATION_JSON_VALUE)
	public void eliminar(@PathVariable Integer id) {
		Paciente espec=service.findById(id);
		/*if(espec==null) {
			throw new ModeloNotFoundException("ID :"+id);
		}
		else*/
		//{
			service.delete(id);
		//}
	}


}
