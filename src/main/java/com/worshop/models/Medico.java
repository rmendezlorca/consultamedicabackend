package com.worshop.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;



@Entity
@Table(name="medico")
public class Medico {

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRun() {
		return run;
	}
	public void setRun(String run) {
		this.run = run;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	@Column(name="run",length=14,nullable=false,unique=true)
	private String run;
	@Column(name="nombres",length=200,nullable=false)
	private String nombres;
	@Column(name="apellidos",length=200,nullable=false)
	private String apellidos;
	@Column(name="direccion",length=200,nullable=false)
	private String direccion;
	@Column(name="telefono",length=100,nullable=false)
	private String telefono;
	@Column(name="email",length=100,nullable=false)
	private String email;
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="medico_especialidad",joinColumns=@JoinColumn(name="id_medico",referencedColumnName="id"),inverseJoinColumns=@JoinColumn(name="id_especialidad",referencedColumnName="id"))
	private List<EspecialidadMedica> especialidadmedicas;
	public List<EspecialidadMedica> getEspecialidadmedicas() {
		return especialidadmedicas;
	}
	public void setEspecialidadmedicas(List<EspecialidadMedica> especialidadmedicas) {
		this.especialidadmedicas = especialidadmedicas;
	}
}
