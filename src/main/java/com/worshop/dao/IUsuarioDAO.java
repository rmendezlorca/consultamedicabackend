package com.worshop.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.worshop.models.Usuario;

public interface IUsuarioDAO extends JpaRepository<Usuario, Integer> {
		
	Usuario findOneByUsername(String username);
}